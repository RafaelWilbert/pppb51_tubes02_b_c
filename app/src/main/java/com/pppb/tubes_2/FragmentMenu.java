package com.pppb.tubes_2;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pppb.tubes_2.databinding.FragmentLoginBinding;
import com.pppb.tubes_2.databinding.FragmentMenuBinding;
import com.pppb.tubes_2.databinding.LoadingPopup2Binding;
import com.pppb.tubes_2.databinding.LoadingPopupBinding;

import org.parceler.Parcel;
import org.parceler.Parcels;

import java.util.ArrayList;

public class FragmentMenu extends Fragment implements View.OnClickListener, RouteTaskPresenter.GetRouteInterface {
    private FragmentMenuBinding binding;
    //    static RouteModel[] arrayRoute;
    private RouteTaskPresenter route;

    private LoadingPopup2Binding binding_popup;
    private PopupWindow popupWindow;

    public static FragmentMenu newInstance(String title) {
        FragmentMenu fragment = new FragmentMenu();
        Bundle args = new Bundle();
        args.putString("title_edit", title);
        fragment.setArguments(args);

        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = FragmentMenuBinding.inflate(inflater, container, false);
        View view = this.binding.getRoot();
        this.binding.btnStart.setOnClickListener(this);
        this.binding.btnHistory.setOnClickListener(this);
        this.binding.btnLogout.setOnClickListener(this);
        Log.d("token test 1", FragmentLogin.token);
        this.route = new RouteTaskPresenter(getActivity(), this);
        Glide.with(this)
                .load("https://c.tenor.com/A2U45SZD3w4AAAAC/back-to-school-school-bus.gif")
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(this.binding.gif);

        binding_popup = LoadingPopup2Binding.inflate(inflater, container, false);
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view == this.binding.btnStart) {
            this.onButtonShowPopupWindowClick(view);
            this.route.execute();
        } else if (view == this.binding.btnHistory) {
            Bundle bundle = new Bundle();
            bundle.putInt("page", 7);
            getParentFragmentManager().setFragmentResult("changePage", bundle);

        } else if (view == this.binding.btnLogout) {
            FragmentLogin.token = null;
            Bundle bundle = new Bundle();
            bundle.putInt("page", 1);
            getParentFragmentManager().setFragmentResult("changePage", bundle);
        }
    }


    public void callback(RouteModel[] arrayRouteList) {
        Toast.makeText(getActivity(), "Success fetch route",
                Toast.LENGTH_LONG).show();
//        arrayRoute=arrayRouteList;
//        for(int i=0; i< arrayRouteList.length;i++){
//            Log.d(String.valueOf(i), arrayRoute[i].getDestination());
//        }
        ArrayList<RouteModel[]> list = new ArrayList<RouteModel[]>();
        list.add(arrayRouteList);
        Parcelable listParcelable = Parcels.wrap(list);
        Fragment fragmentGet = new FragmentRoutes();
        Bundle bundle = new Bundle();

        bundle.putParcelable("route", listParcelable);

        fragmentGet.setArguments(bundle);

        bundle.putInt("page", 3);
        getParentFragmentManager().setFragmentResult("changePage", bundle);
        getParentFragmentManager().setFragmentResult("addRoute", bundle);
        popupWindow.dismiss();
    }

    @Override
    public void routeError() {
        Toast.makeText(getActivity(), "Gagal fetch route",
                Toast.LENGTH_LONG).show();
        popupWindow.dismiss();

    }

    public void closeSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    public void onButtonShowPopupWindowClick(View view) {


        View popupView = this.binding_popup.getRoot();

        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        popupWindow = new PopupWindow(popupView, width, height, focusable);


        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

    }
}
