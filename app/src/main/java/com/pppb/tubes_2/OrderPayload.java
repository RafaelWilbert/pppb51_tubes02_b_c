package com.pppb.tubes_2;

public class OrderPayload {
    String order_id;
    int[] seats;
    public OrderPayload(String order_id, int[] seats){
        this.order_id= order_id;
        this.seats = seats;
    }

    public int[] getSeats() {
        return seats;
    }

    public void setSeats(int[] seats) {
        this.seats = seats;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}
