package com.pppb.tubes_2;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;


import com.pppb.tubes_2.databinding.ListHistoryBinding;

import java.util.Arrays;
import java.util.List;

public class AdapterHistory extends BaseAdapter {
    private Activity activity;
    public static List<HistoryPayload> list;
    private ListHistoryBinding binding;

    public AdapterHistory(Activity activity) {
        this.activity = activity;
        this.list = new ArrayList<>();
    }

    public void setList(List list) {
        this.list = list;

        this.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int i) {
        return this.list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        AdapterHistory.ViewHolder vh;

        if (convertView == null) {
            binding = ListHistoryBinding.inflate((this.activity.getLayoutInflater()));
            convertView = binding.getRoot();
            vh = new AdapterHistory.ViewHolder(binding, this, i);
            convertView.setTag(vh);
        } else {
            vh = (AdapterHistory.ViewHolder) convertView.getTag();
        }

        HistoryPayload currentPayload = (HistoryPayload) getItem(i);
        String source = currentPayload.getSource();
        String destination = currentPayload.getDestination();
        String order_datetime = currentPayload.getOrder_datetime();
        String seats = String.valueOf(Arrays.toString(currentPayload.getSeats()));
        String datetime = currentPayload.getCourse_datetime();
        String vehicle = currentPayload.getVehicle();


        vh.setList(source, destination, order_datetime,datetime,seats,vehicle);

        return convertView;
    }

    class ViewHolder {
        private ListHistoryBinding binding;
        private AdapterHistory adapter;
        private int i;

        public ViewHolder(ListHistoryBinding binding, AdapterHistory adapter, int i) {
            this.binding = binding;
            this.adapter = adapter;

            this.i = i;
        }

        public void setList(String source, String destination, String order_datetime,String course_datetime, String seats, String vehicle) {
            this.binding.tvSource.setText(source);
            this.binding.tvDestination.setText(destination);
            this.binding.tvJamPesan.setText(order_datetime);
            this.binding.tvJamBerangkat.setText(course_datetime);
            this.binding.tvSeats.setText(seats);
            this.binding.tvVehicle.setText(vehicle);
        }

    }
}

