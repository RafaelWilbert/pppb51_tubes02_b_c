package com.pppb.tubes_2;

public class LoginResult {
    private String token ;
    private String message;

    public LoginResult(String token, String message) {
        this.token = token;
        this.message = message;
    }

    public String getToken(){
        return this.token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
