package com.pppb.tubes_2;


import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.pppb.tubes_2.databinding.FragmentHistoryBinding;
import com.pppb.tubes_2.databinding.LoadingPopup2Binding;
import com.pppb.tubes_2.databinding.LoadingPopup4Binding;

import java.util.ArrayList;
import java.util.List;

public class FragmentHistory extends Fragment implements HistoryTaskPresenter.HistoryTaskInterface, View.OnClickListener {
    protected FragmentHistoryBinding binding;
    protected HistoryTaskPresenter presenter;
    private AdapterHistory adapter;
    private final String limit = "8";
    private final String offset = "0";

    public static FragmentHistory newInstance(String title) {
        FragmentHistory fragment = new FragmentHistory();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = FragmentHistoryBinding.inflate(inflater, container, false);
        View view = this.binding.getRoot();
        this.binding.btnHome.setOnClickListener(this);
        this.presenter = new HistoryTaskPresenter(getActivity(), this);
        presenter.execute("8", "0");
        this.adapter = new AdapterHistory(this.getActivity());
        this.adapter.notifyDataSetChanged();
        return view;
    }


    @Override
    public void callback(HistoryPayload[] result) {
        List<HistoryPayload> list = makeList(result);
        this.adapter.setList(list);
        this.binding.lstOrder.setAdapter(this.adapter);
        Toast.makeText(getActivity(), "Success mendapatkan history ",
                Toast.LENGTH_LONG).show();

    }

    @Override
    public void routeError() {
        Toast.makeText(getActivity(), "Gagal mendapatkan history ",
                Toast.LENGTH_LONG).show();

    }

    public List<HistoryPayload> makeList(HistoryPayload[] result) {
        ArrayList<HistoryPayload> list = new ArrayList();
        for (int i = 0; i < result.length; i++) {
            list.add(result[i]);
        }
        return list;
    }

    @Override
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt("page", 2);
        getParentFragmentManager().setFragmentResult("changePage", bundle);

    }
}
