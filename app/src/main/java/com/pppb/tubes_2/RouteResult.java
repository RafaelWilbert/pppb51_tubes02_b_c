package com.pppb.tubes_2;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@Parcel
public class RouteResult {
    RouteModel[] payload;
    @ParcelConstructor
    public RouteResult(RouteModel[] payload){
        this.payload = payload;
    }

    public void setPayload(RouteModel[] payload) {

        this.payload = payload;
    }

    public RouteModel[] getPayload() {
        return payload;
    }

    }


