package com.pppb.tubes_2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentResultListener;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.pppb.tubes_2.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements FragmentResultListener {
    private ActivityMainBinding binding;
    //fragment-fragment
    protected FragmentLogin fl;
    protected FragmentMenu fm;
    protected FragmentRoutes fr;
    protected FragmentTempatDudukBesar ftdb;
    protected FragmentTempatDudukKecil ftdk;
    protected FragmentDetailPesanan fdp;
    protected FragmentHistory fh;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = ActivityMainBinding.inflate(this.getLayoutInflater());
        setContentView(this.binding.getRoot());
        this.fl = FragmentLogin.newInstance("Fragment Login");
        this.fm = FragmentMenu.newInstance("Fragment Menu");
        this.fr = FragmentRoutes.newInstance("Fragment Routes");
        this.ftdb = FragmentTempatDudukBesar.newInstance("Fragment Tempat Duduk Besar");
        this.ftdk = FragmentTempatDudukKecil.newInstance("Frament Temapt Duduk Kecil");
        this.fdp = FragmentDetailPesanan.newInstance("Fragment Detail Pesanan");
        this.fh = FragmentHistory.newInstance("Fragment History");

        this.fragmentManager = this.getSupportFragmentManager();
        this.fragmentManager.beginTransaction().add(R.id.fragment_container, fl).addToBackStack(null).commit();

        this.fragmentManager.setFragmentResultListener("changePage", this, this);

        this.fragmentManager.setFragmentResultListener("closeApplication", this, this);

    }


    @Override
    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
        if (requestKey.equals("changePage")) {
            int page = result.getInt("page");
            changePage(page);
        } else if (requestKey.equals("closeApplication")) {
//          closeApplication();
        }
    }


    private void changePage(int page) {
        FragmentTransaction ft = this.fragmentManager.beginTransaction();
        if (page == 1) {
            if (this.fl.isAdded()) {
                ft.show(this.fl);
            } else {
                ft.add(R.id.fragment_container, this.fl);
            }
            if (this.fm.isAdded()) {
                ft.hide(this.fm);
            }
            if (this.fr.isAdded()) {
                ft.hide(this.fr);
            }
            if (this.ftdb.isAdded()) {
                ft.hide(this.ftdb);
            }
            if (this.ftdk.isAdded()) {
                ft.hide(this.ftdk);
            }
            if (this.fdp.isAdded()) {
                ft.hide(this.fdp);
            }

            if (this.fh.isAdded()) {
                ft.hide(this.fh);
            }


        } else if (page == 2) {

            if (this.fm.isAdded()) {
                ft.show(this.fm);
            } else {
                ft.add(R.id.fragment_container, this.fm).addToBackStack(null);
            }
            if (this.fl.isAdded()) {
                ft.hide(this.fl);
            }

            if (this.fr.isAdded()) {
                ft.hide(this.fr);
            }
            if (this.ftdb.isAdded()) {
                ft.hide(this.ftdb);
            }
            if (this.ftdk.isAdded()) {
                ft.hide(this.ftdk);
            }
            if (this.fdp.isAdded()) {
                ft.hide(this.fdp);
            }

            if (this.fh.isAdded()) {
                ft.hide(this.fh);
            }

        } else if (page == 3) {

            if (this.fr.isAdded()) {
                ft.show(this.fr);
            } else {
                ft.add(R.id.fragment_container, this.fr).addToBackStack(null);
            }
            if (this.fm.isAdded()) {
                ft.hide(this.fm);
            }
            if (this.ftdb.isAdded()) {
                ft.hide(this.ftdb);
            }
            if (this.ftdk.isAdded()) {
                ft.hide(this.ftdk);
            }
            if (this.fdp.isAdded()) {
                ft.hide(this.fdp);
            }

            if (this.fh.isAdded()) {
                ft.hide(this.fh);
            }

        } else if (page == 4) {
            if (this.ftdb.isAdded()) {
                ft.show(this.ftdb);
            } else {
                ft.add(R.id.fragment_container, this.ftdb).addToBackStack(null);
            }
            if (this.fm.isAdded()) {
                ft.hide(this.fm);
            }
            if (this.fr.isAdded()) {
                ft.hide(this.fr);
            }
            if (this.ftdk.isAdded()) {
                ft.hide(this.ftdk);
            }
            if (this.fdp.isAdded()) {
                ft.hide(this.fdp);
            }

            if (this.fh.isAdded()) {
                ft.hide(this.fh);
            }


        } else if (page == 5) {
            if (this.ftdk.isAdded()) {
                ft.show(this.ftdk);
            } else {
                ft.add(R.id.fragment_container, this.ftdk).addToBackStack(null);
            }
            if (this.fm.isAdded()) {
                ft.hide(this.fm);
            }
            if (this.fr.isAdded()) {
                ft.hide(this.fr);
            }
            if (this.ftdb.isAdded()) {
                ft.hide(this.ftdb);
            }
            if (this.fdp.isAdded()) {
                ft.hide(this.fdp);
            }

            if (this.fh.isAdded()) {
                ft.hide(this.fh);
            }

        } else if (page == 6) {
            if (this.fdp.isAdded()) {
                ft.show(this.fdp);
            } else {
                ft.add(R.id.fragment_container, this.fdp).addToBackStack(null);
            }
            if (this.fm.isAdded()) {
                ft.hide(this.fm);
            }
            if (this.fr.isAdded()) {
                ft.hide(this.fr);
            }
            if (this.ftdb.isAdded()) {
                ft.hide(this.ftdb);
            }
            if (this.ftdk.isAdded()) {
                ft.hide(this.ftdk);
            }


            if (this.fh.isAdded()) {
                ft.hide(this.fh);
            }

        } else if (page == 7) {
            if (this.fh.isAdded()) {
                ft.remove(this.fh);
                this.fh = FragmentHistory.newInstance("Fragment History");
                ft.add(R.id.fragment_container, this.fh).addToBackStack(null);

            } else {

                ft.add(R.id.fragment_container, this.fh).addToBackStack(null);
            }
            if (this.fm.isAdded()) {
                ft.hide(this.fm);
            }
            if (this.fr.isAdded()) {
                ft.hide(this.fr);
            }
            if (this.ftdb.isAdded()) {
                ft.hide(this.ftdb);
            }
            if (this.ftdk.isAdded()) {
                ft.hide(this.ftdk);
            }
            if (this.fdp.isAdded()) {
                ft.hide(this.fdp);
            }

        }
        ft.commit();
        this.binding.drawerLayout.closeDrawers();
    }

}


