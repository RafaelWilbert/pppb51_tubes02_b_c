package com.pppb.tubes_2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import com.pppb.tubes_2.databinding.FragmentDetailPesananBinding;

import org.parceler.Parcels;

import java.util.ArrayList;

public class FragmentDetailPesanan extends Fragment implements OrderTaskPresenter.OrderTaskInterface, View.OnClickListener {
    private OrderTaskPresenter presenter;
    private FragmentDetailPesananBinding binding;
    private OrderModel order;
    private getSeats detail;

    public FragmentDetailPesanan() {
    }

    public static FragmentDetailPesanan newInstance(String param1) {
        FragmentDetailPesanan fragment = new FragmentDetailPesanan();
        Bundle args = new Bundle();
        args.putString("title_edit", param1);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.binding = FragmentDetailPesananBinding.inflate(inflater, container, false);
        View view = this.binding.getRoot();
        this.presenter = new OrderTaskPresenter(getContext(), this);
        this.binding.btnDetailPesenId.setOnClickListener(this);
        getParentFragmentManager().setFragmentResultListener("pesan", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                ArrayList<OrderModel> list_order = Parcels.unwrap(result.getParcelable("order"));
                order = list_order.get(0);


                ArrayList<getSeats> list_detail = Parcels.unwrap(result.getParcelable("detail"));
                detail = list_detail.get(0);


                binding.tvDariId.setText(detail.getSource());
                binding.tvKeId.setText(detail.getDestination());
                binding.tvTanggalId.setText(detail.getDatetime().substring(0,10));

                binding.tvWaktuId.setText(detail.getDatetime().substring(11,16));
                String[] arrSeats = order.getSeats().split(",");
                binding.tvSeatId.setText(order.getSeats());
                binding.tvHargaId.setText(String.valueOf(calcHarga(detail.getFee(),arrSeats.length)));
            }
        });
        this.binding.btnDetailPesenId.setOnClickListener(this);
        this.binding.btnBack.setOnClickListener(this);
        return view;
    }

    private int calcHarga(int fee, int seats) {
        return fee * seats;
    }

    @Override
    public void callback(OrderPayload[] payload) {
        Toast.makeText(getActivity(), "Success place order",
                Toast.LENGTH_LONG).show();
    order=null;
        FragmentTempatDudukBesar.arrBuatPilihKursi = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        FragmentTempatDudukBesar.arr =new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        FragmentTempatDudukKecil.arrBuatPilihKursi=new int[]{0, 0, 0, 0, 0, 0};
        FragmentTempatDudukKecil.arr=new int[]{0, 0, 0, 0, 0, 0};
        Bundle bundle = new Bundle();



        bundle.putInt("page", 7);
        getParentFragmentManager().setFragmentResult("changePage", bundle);

    }

    @Override
    public void routeError() {
        Toast.makeText(getActivity(), "Gagal place order",
                Toast.LENGTH_LONG).show();

    }

    @Override
    public void onClick(View view) {
    if(view ==this.binding.btnDetailPesenId) {
        presenter.execute(order);
    }
    else if (view ==this.binding.btnBack){

        Bundle bundle = new Bundle();


        if(detail.getVehicle().equalsIgnoreCase("Small")){
            bundle.putInt("page", 5);
        }
        else if(detail.getVehicle().equalsIgnoreCase("Large")){
            bundle.putInt("page", 4);
        }

        getParentFragmentManager().setFragmentResult("changePage", bundle);
    }
    }
}
