package com.pppb.tubes_2;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.renderscript.Sampler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentResultListener;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.pppb.tubes_2.databinding.FragmentRoutesBinding;
import com.pppb.tubes_2.databinding.LoadingPopup2Binding;
import com.pppb.tubes_2.databinding.LoadingPopup3Binding;


import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Arrays;


import fr.ganfra.materialspinner.MaterialSpinner;

public class FragmentRoutes extends Fragment implements FragmentRoutesPresenter.FragmentRoutesInterface, View.OnClickListener, CalendarDatePickerDialogFragment.OnDateSetListener, RadialTimePickerDialogFragment.OnTimeSetListener, CourseTaskPresenter.GetCourseInterface {
    private FragmentRoutesBinding binding;
    public static String token;
    private RouteModel[] arrayRouteList;
    private String[] from;
    private String[] to;
    private String[] listVehicle = {"Large", "Small"};
    private ArrayAdapter adapterFrom;
    private ArrayAdapter adapterTo;
    private ArrayAdapter adapterVehicle;
    MaterialSpinner spinner1;
    MaterialSpinner spinner2;
    MaterialSpinner spinner3;
    private static final String FRAG_TAG_DATE_PICKER = "fragment_date_picker_name";
    private static final String FRAG_TAG_TIME_PICKER = "timePickerDialogFragment";
    private FragmentRoutesPresenter presenter;
    private CourseTaskPresenter presenter_task;
    private LoadingPopup3Binding binding_popup;
    private PopupWindow popupWindow;
    //untuk request get
    private String source;
    private String destination;
    private String date;
    private String hour;
    private String vehicle;

    public static FragmentRoutes newInstance(String title) {
        FragmentRoutes fragment = new FragmentRoutes();
        Bundle args = new Bundle();
        args.putString("title_edit", title);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = FragmentRoutesBinding.inflate(inflater, container, false);
        getParentFragmentManager().setFragmentResultListener("GetRoute", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        });
        this.presenter = new FragmentRoutesPresenter(this);
        this.presenter_task = new CourseTaskPresenter(getActivity(), this);
        spinner1 = this.binding.spinnerDari;
        spinner2 = this.binding.spinnerKe;
        spinner3 = this.binding.spinnerSize;
        View view = this.binding.getRoot();
        binding_popup = LoadingPopup3Binding.inflate(inflater, container, false);
        this.binding.btnCari.setOnClickListener(this);
        getParentFragmentManager().setFragmentResultListener("addRoute", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                ArrayList<RouteModel[]> list = Parcels.unwrap(result.getParcelable("route"));
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                arrayRouteList = list.get(0);
                setSpinner();
            }
        });
        this.binding.btnTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(FragmentRoutes.this);
                cdp.show(getActivity().getSupportFragmentManager(), FRAG_TAG_DATE_PICKER);
            }
        });
        this.binding.btnJam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                        .setOnTimeSetListener(FragmentRoutes.this);
                rtpd.show(getActivity().getSupportFragmentManager(), FRAG_TAG_TIME_PICKER);
            }
        });
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i >= 0) {
                    source = from[i];
                    binding.tvDari.setText(source);
                } else {
                    source = null;
                    binding.tvDari.setText(source);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i >= 0) {
                    destination = to[i];
                    binding.tvKe.setText(destination);
                } else {
                    destination = null;
                    binding.tvKe.setText(destination);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i >= 0) {
                    vehicle = listVehicle[i];
                    binding.tvSize.setText(vehicle);
                } else {
                    vehicle = null;
                    binding.tvSize.setText(vehicle);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });
        this.binding.btnKembali.setOnClickListener(this);
        return view;
    }

    public void setSpinner() {
        from = new String[arrayRouteList.length];
        to = new String[arrayRouteList.length];
        for (int i = 0; i < arrayRouteList.length; i++) {
            from[i] = arrayRouteList[i].getSource();
            to[i] = arrayRouteList[i].getDestination();

        }

        this.presenter.arrayNoDuplicate(from, 0);
        this.presenter.arrayNoDuplicate(to, 1);

        adapterVehicle = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, listVehicle);
        adapterVehicle.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner3.setAdapter(adapterVehicle);

    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        this.date = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(year);
        this.binding.tvTanggal.setText(this.date);
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        String jam = String.valueOf(hourOfDay);
        Log.d("hour", String.valueOf(hourOfDay));
        if (jam.length() < 2) {

            jam = "0" + jam;
        } else {
            this.hour = jam.substring(0, 2);
        }
        this.binding.tvJam.setText(getString(R.string.radial_time_picker_result_value, hourOfDay, minute));

    }

    @Override
    public void callback(String[] array, int i) {
        if (i == 0) {
            from = array;
            adapterFrom = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, array);
            adapterFrom.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner1.setAdapter(adapterFrom);
        } else {
            to = array;
            adapterTo = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, array);
            adapterTo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner2.setAdapter(adapterTo);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == this.binding.btnCari) {
            this.onButtonShowPopupWindowClick(view);
            CourseModel course = new CourseModel(source, destination, vehicle, date, hour);
            this.presenter_task.execute(course);

        } else if (this.binding.btnKembali == view) {
            Bundle bundle = new Bundle();
            bundle.putInt("page", 2);
            getParentFragmentManager().setFragmentResult("changePage", bundle);
        }
    }

    @Override
    public void callback(getSeats[] res) {

        popupWindow.dismiss();
        Toast.makeText(getActivity(), "Success fetch seats",
                Toast.LENGTH_LONG).show();


        ArrayList<getSeats[]> list = new ArrayList<getSeats[]>();
        list.add(res);
        Parcelable listParcelable = Parcels.wrap(list);
        Fragment fragmentGet = new FragmentRoutes();
        Bundle bundle = new Bundle();

        bundle.putParcelable("route", listParcelable);

        fragmentGet.setArguments(bundle);
        String vehicle = res[0].getVehicle();
        if (vehicle.equals("Large")) {
            bundle.putInt("page", 4);
            getParentFragmentManager().setFragmentResult("changePage", bundle);
            getParentFragmentManager().setFragmentResult("addSeats", bundle);
        } else if (vehicle.equals("Small")) {
            bundle.putInt("page", 5);
            getParentFragmentManager().setFragmentResult("changePage", bundle);
            getParentFragmentManager().setFragmentResult("addSeats", bundle);
        }


    }

    @Override
    public void routeError() {
        Toast.makeText(getActivity(), "Gagal fetch seats",
                Toast.LENGTH_LONG).show();
        popupWindow.dismiss();
    }

    public void onButtonShowPopupWindowClick(View view) {
        View popupView = this.binding_popup.getRoot();
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

    }
}
