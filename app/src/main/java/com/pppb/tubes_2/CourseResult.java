package com.pppb.tubes_2;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;
@Parcel
public class CourseResult {
    getSeats[] payload;
    @ParcelConstructor
    public CourseResult(getSeats[] payload){
        this.payload = payload;
    }

    public void setPayload(getSeats[] payload) {

        this.payload = payload;
    }

    public getSeats[] getPayload() {
        return payload;
    }

}
