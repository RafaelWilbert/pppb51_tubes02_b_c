package com.pppb.tubes_2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.pppb.tubes_2.databinding.FragmentTempatDudukKecilBinding;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Arrays;

public class FragmentTempatDudukKecil extends Fragment implements View.OnTouchListener, View.OnClickListener {
    private FragmentTempatDudukKecilBinding binding;

    static int[] arr;
    static int[] arrBuatPilihKursi;

    private Canvas mCanvas;
    private Paint kotakKosong;
    private Paint kotakIsi;
    private Paint kotakMilih;
    private getSeats[] arraySeatsList;
    private getSeats detail;

    //atribut untuk passing
    private String course_id;
    private String seats;

    public FragmentTempatDudukKecil() {

    }


    public static FragmentTempatDudukKecil newInstance(String param1) {
        FragmentTempatDudukKecil fragment = new FragmentTempatDudukKecil();
        Bundle args = new Bundle();
        args.putString("title_edit", param1);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.binding = FragmentTempatDudukKecilBinding.inflate(inflater, container, false);
        View view = this.binding.getRoot();
        this.arrBuatPilihKursi = new int[]{0, 0, 0, 0, 0, 0};
        this.arr = new int[]{0, 0, 0, 0, 0, 0};
        getParentFragmentManager().setFragmentResultListener("addSeats", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                ArrayList<getSeats[]> list = Parcels.unwrap(result.getParcelable("route"));
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                arraySeatsList = list.get(0);
                detail = arraySeatsList[0];
                String destination = detail.getDestination();
                String source = detail.getSource();
                binding.tvRute.setText(source + " - " + destination);
                int[] seats_taken = detail.getSeats();
                setTaken(seats_taken);
                initiateCanvas();
                cekStatus();
                course_id = detail.getCourse_id();
                Log.d("test debug ", Arrays.toString(seats_taken));
            }
        });
        this.binding.imageView.setOnTouchListener(this);
        this.binding.btnPesenId.setOnClickListener(this);
        this.binding.btnBack.setOnClickListener(this);

        return view;
    }

    private void setTaken(int[] seats_taken) {
        for (int i = 0; i < seats_taken.length; i++) {
            String debug = Arrays.toString(seats_taken) + Arrays.toString(arr);
            Log.d("taken", debug);
            int taken = seats_taken[i] - 1;
            arr[taken] = 1;

        }
    }

    public void initiateCanvas() {
        Bitmap mBitmap = Bitmap.createBitmap(1080, 1220, Bitmap.Config.ARGB_8888);

        this.binding.imageView.setImageBitmap(mBitmap);

        mCanvas = new Canvas(mBitmap);

        int warnaBegron = ResourcesCompat.getColor(getResources(), R.color.teal_200, null);
        mCanvas.drawColor(warnaBegron);

        kotakIsi = new Paint();
        kotakKosong = new Paint();
        kotakMilih = new Paint();

        int warnaKotakKosong = ResourcesCompat.getColor(getResources(), R.color.white, null);
        int warnaKotakIsi = ResourcesCompat.getColor(getResources(), R.color.bpRed, null);
        int warnakotakMilih = ResourcesCompat.getColor(getResources(), R.color.purple_500, null);

        kotakIsi.setColor(warnaKotakIsi);
        kotakKosong.setColor(warnaKotakKosong);
        kotakMilih.setColor(warnakotakMilih);

        this.binding.imageView.invalidate();
    }

    public void cekStatus() {
        for (int i = 0; i < arr.length; i++) {
            if (i == 0) {
                if (arr[i] == 0) {
                    Rect rect = new Rect(50, 350, 200, 500);
                    mCanvas.drawRect(rect, kotakKosong);
                } else if (arr[i] == 1) {
                    Rect rect = new Rect(50, 350, 200, 500);
                    mCanvas.drawRect(rect, kotakIsi);
                }
            } else if (i == 1) {
                if (arr[i] == 0) {
                    Rect rect = new Rect(250, 350, 400, 500);
                    mCanvas.drawRect(rect, kotakKosong);
                } else if (arr[i] == 1) {
                    Rect rect = new Rect(250, 350, 400, 500);
                    mCanvas.drawRect(rect, kotakIsi);
                }
            } else if (i == 2) {
                if (arr[i] == 0) {
                    Rect rect = new Rect(650, 350, 800, 500);
                    mCanvas.drawRect(rect, kotakKosong);
                } else if (arr[i] == 1) {
                    Rect rect = new Rect(650, 350, 800, 500);
                    mCanvas.drawRect(rect, kotakIsi);
                }
            } else if (i == 3) {
                if (arr[i] == 0) {
                    Rect rect = new Rect(850, 350, 1000, 500);
                    mCanvas.drawRect(rect, kotakKosong);
                } else if (arr[i] == 1) {
                    Rect rect = new Rect(850, 350, 1000, 500);
                    mCanvas.drawRect(rect, kotakIsi);
                }
            } else if (i == 4) {
                if (arr[i] == 0) {
                    Rect rect = new Rect(350, 700, 500, 850);
                    mCanvas.drawRect(rect, kotakKosong);
                } else if (arr[i] == 1) {
                    Rect rect = new Rect(350, 700, 500, 850);
                    mCanvas.drawRect(rect, kotakIsi);
                }
            } else if (i == 5) {
                if (arr[i] == 0) {
                    Rect rect = new Rect(550, 700, 700, 850);
                    mCanvas.drawRect(rect, kotakKosong);
                } else if (arr[i] == 1) {
                    Rect rect = new Rect(550, 700, 700, 850);
                    mCanvas.drawRect(rect, kotakIsi);
                }
            }
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();

        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                if (x >= 50 && x <= 200 && y >= 350 && y <= 500) {

                    if (arr[0] == 0) {

                        if (arrBuatPilihKursi[0] == 0) {
                            Rect rect = new Rect(50, 350, 200, 500);
                            mCanvas.drawRect(rect, kotakMilih);
                            arrBuatPilihKursi[0] = 1;
                        } else {
                            Rect rect = new Rect(50, 350, 200, 500);
                            mCanvas.drawRect(rect, kotakKosong);
                            arrBuatPilihKursi[0] = 0;
                        }
                    } else {
                        Toast.makeText(getContext(), "Kursi Sudah Ada Yang Pesan", Toast.LENGTH_SHORT).show();
                    }

                } else if (x >= 250 && x <= 400 && y >= 350 && y <= 500) {

                    if (arr[1] == 0) {

                        if (arrBuatPilihKursi[1] == 0) {
                            Rect rect = new Rect(250, 350, 400, 500);
                            mCanvas.drawRect(rect, kotakMilih);
                            arrBuatPilihKursi[1] = 1;
                        } else {
                            Rect rect = new Rect(250, 350, 400, 500);
                            mCanvas.drawRect(rect, kotakKosong);
                            arrBuatPilihKursi[1] = 0;
                        }
                    } else {
                        Toast.makeText(getContext(), "Kursi Sudah Ada Yang Pesan", Toast.LENGTH_SHORT).show();
                    }
                } else if (x >= 650 && x <= 800 && y >= 350 && y <= 500) {

                    if (arr[2] == 0) {

                        if (arrBuatPilihKursi[2] == 0) {
                            Rect rect = new Rect(650, 350, 800, 500);
                            mCanvas.drawRect(rect, kotakMilih);
                            arrBuatPilihKursi[2] = 1;
                        } else {
                            Rect rect = new Rect(650, 350, 800, 500);
                            mCanvas.drawRect(rect, kotakKosong);
                            arrBuatPilihKursi[2] = 0;
                        }
                    } else {
                        Toast.makeText(getContext(), "Kursi Sudah Ada Yang Pesan", Toast.LENGTH_SHORT).show();
                    }
                } else if (x >= 850 && x <= 1000 && y >= 350 && y <= 500) {

                    if (arr[3] == 0) {

                        if (arrBuatPilihKursi[3] == 0) {
                            Rect rect = new Rect(850, 350, 1000, 500);
                            mCanvas.drawRect(rect, kotakMilih);
                            arrBuatPilihKursi[3] = 1;
                        } else {
                            Rect rect = new Rect(850, 350, 1000, 500);
                            mCanvas.drawRect(rect, kotakKosong);
                            arrBuatPilihKursi[3] = 0;
                        }
                    } else {
                        Toast.makeText(getContext(), "Kursi Sudah Ada Yang Pesan", Toast.LENGTH_SHORT).show();
                    }
                } else if (x >= 350 && x <= 500 && y >= 700 && y <= 850) {

                    if (arr[4] == 0) {

                        if (arrBuatPilihKursi[4] == 0) {
                            Rect rect = new Rect(350, 700, 500, 850);
                            mCanvas.drawRect(rect, kotakMilih);
                            arrBuatPilihKursi[4] = 1;
                        } else {
                            Rect rect = new Rect(350, 700, 500, 850);
                            mCanvas.drawRect(rect, kotakKosong);
                            arrBuatPilihKursi[4] = 0;
                        }
                    } else {
                        Toast.makeText(getContext(), "Kursi Sudah Ada Yang Pesan", Toast.LENGTH_SHORT).show();
                    }
                } else if (x >= 550 && x <= 700 && y >= 700 && y <= 850) {

                    if (arr[5] == 0) {

                        if (arrBuatPilihKursi[5] == 0) {
                            Rect rect = new Rect(550, 700, 700, 850);
                            mCanvas.drawRect(rect, kotakMilih);
                            arrBuatPilihKursi[5] = 1;
                        } else {
                            Rect rect = new Rect(550, 700, 700, 850);
                            mCanvas.drawRect(rect, kotakKosong);
                            arrBuatPilihKursi[5] = 0;
                        }
                    } else {
                        Toast.makeText(getContext(), "Kursi Sudah Ada Yang Pesan", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
        this.binding.imageView.invalidate();
        return false;
    }

    @Override
    public void onClick(View view) {
        if (view == this.binding.btnPesenId) {
            String string = "";
            int count = 0;
            for (int i = 0; i < arrBuatPilihKursi.length; i++) {
                if (arrBuatPilihKursi[i] == 1) {
                    string = string + (i + 1) + ",";
                    count++;
                }
            }
            if (count == 0) {
                Toast.makeText(getContext(), "Silahkan memilih kursi yang tersedia", Toast.LENGTH_SHORT).show();
            } else {
                String stringUdaDiPotong = "";
                if (string.length() > 0) {

                    stringUdaDiPotong = string.substring(0, string.length() - 1);

                }
                Log.d("hasil kursi", "onClick: " + stringUdaDiPotong);
                if (stringUdaDiPotong == "") {
                    stringUdaDiPotong = "0";
                }
                ArrayList<getSeats> list_detail = new ArrayList<getSeats>();
                list_detail.add(detail);

                Parcelable listDetail = Parcels.wrap(list_detail);
                OrderModel order = new OrderModel(detail.getCourse_id(), stringUdaDiPotong);
                ArrayList<OrderModel> list_order = new ArrayList<OrderModel>();
                list_order.add(order);

                Bundle bundle = new Bundle();
                Parcelable listOrder = Parcels.wrap(list_order);
                bundle.putParcelable("detail", listDetail);
                bundle.putParcelable("order", listOrder);
                bundle.putInt("page", 6);
                getParentFragmentManager().setFragmentResult("changePage", bundle);
                getParentFragmentManager().setFragmentResult("pesan", bundle);
            }

        } else if (view == this.binding.btnBack) {
            Bundle bundle = new Bundle();
            bundle.putInt("page", 3);
            getParentFragmentManager().setFragmentResult("changePage", bundle);
            this.arrBuatPilihKursi = new int[]{0, 0, 0, 0, 0, 0};
            this.arr = new int[]{0, 0, 0, 0, 0, 0};
        }
    }


}