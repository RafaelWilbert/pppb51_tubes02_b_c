package com.pppb.tubes_2;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

@Parcel
public class getSeats {
    String course_id;
    String source;
    String destination;
    String datetime;
    String vehicle;
    int num_seats;
    int[] seats;
    int fee;

    @ParcelConstructor
    public getSeats(String course_id, String source, String destination, String datetime
            , String vehicle, int num_seats, int[] seats, int fee) {
        this.course_id = course_id;
        this.source = source;
        this.destination = destination;
        this.datetime = datetime;
        this.vehicle = vehicle;
        this.num_seats = num_seats;
        this.seats = seats;
        this.fee = fee;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setNum_seats(int num_seats) {
        this.num_seats = num_seats;
    }

    public void setSeats(int[] seats) {
        this.seats = seats;
    }

    public int getNum_seats() {
        return num_seats;
    }

    public String getVehicle() {
        return vehicle;
    }

    public String getDestination() {
        return destination;
    }

    public String getSource() {
        return source;
    }

    public int getFee() {
        return fee;
    }

    public int[] getSeats() {
        return seats;
    }

    public String getCourse_id() {
        return course_id;
    }

    public String getDatetime() {
        return datetime;
    }

}
