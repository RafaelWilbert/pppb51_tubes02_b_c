package com.pppb.tubes_2;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginTaskPresenter {
    final String BASE_URL = "https://devel.loconode.com/pppb/v1/authenticate";
    private Context context;
    private Gson gson;
    private PostCalculateInterface IMainActivity;
    private JSONObject objJSON;
    public LoginTaskPresenter(Context context, PostCalculateInterface IMainActivity) {
        this.context = context;
        this.IMainActivity= IMainActivity;
    }

    public void execute(String username, String password) {
        gson = new Gson();
        LoginModel login = new LoginModel(username, password);
        String toJson = gson.toJson(login);
        try {
            objJSON=new JSONObject(toJson);

            Log.d("TAG", toJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        this.callVolley(objJSON);
        //  this.callVolley(expr,precision);
    }

    private void callVolley(JSONObject toJson) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL, toJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Respon", response.toString());
                        processResult(response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Respon Error", error.toString());

                processError();
            }
        });

        requestQueue.add(jsonRequest);
    }
    private void processError(){
    this.IMainActivity.loginError();
    }
    private void processResult(String json){

        LoginResult result= gson.fromJson(json,LoginResult.class);
        String token = result.getToken();
        String message =result.getMessage();

        this.IMainActivity.callback(token,message);
    }
    interface PostCalculateInterface {
        public void callback(String token ,String message);
        public void loginError();
    }
}
