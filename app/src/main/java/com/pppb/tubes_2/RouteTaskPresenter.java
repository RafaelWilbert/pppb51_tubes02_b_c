package com.pppb.tubes_2;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class RouteTaskPresenter {
    final String BASE_URL = "https://devel.loconode.com/pppb/v1/routes";
    private Context context;
    private Gson gson;
    private RouteTaskPresenter.GetRouteInterface IMainActivity;
    private JSONObject objJSON;
    public static String token;
    public RouteTaskPresenter(Context context, RouteTaskPresenter.GetRouteInterface IMainActivity) {
        this.context = context;
        this.IMainActivity = IMainActivity;
    }

    public void execute() {
        Log.d("TAG", FragmentLogin.token);
        this.callVolley(objJSON);
    }

    private void callVolley(JSONObject toJson) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, BASE_URL, toJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        processResult(response.toString());
                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Respon Error", error.toString());

                processError();

            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + FragmentLogin.token);
                return headers;
            }
        };


        requestQueue.add(jsonRequest);
    }

    private void processError() {
        this.IMainActivity.routeError();
    }

    private void processResult(String json) {

        RouteResult result = new Gson().fromJson(json, RouteResult.class);
        RouteModel[] array = result.getPayload();
        this.IMainActivity.callback(array);
    }

    interface GetRouteInterface {
        public void callback(RouteModel[] result);

        public void routeError();
    }
}
