package com.pppb.tubes_2;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HistoryTaskPresenter {

    private Context context;
    private Gson gson;
    private HistoryTaskPresenter.HistoryTaskInterface ui;
    private JSONObject objJSON;
    public static String token;

    public HistoryTaskPresenter(Context context, HistoryTaskPresenter.HistoryTaskInterface ui) {
        this.context = context;
        this.ui = ui;
    }

    public void execute(String limit, String offset) {
        this.callVolley(objJSON, limit, offset);
    }

    private void callVolley(JSONObject toJson, String limit, String offset) {

        String BASE_URL = "https://devel.loconode.com/pppb/v1/orders?limit=" + limit + "&offset=" + offset;
        Log.d("url", BASE_URL);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET, BASE_URL, toJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        processResult(response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Respon Error", error.toString());

                processError();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + FragmentLogin.token);
                return headers;
            }
        };
        requestQueue.add(myReq);
    }

    private void processError() {
        this.ui.routeError();
    }

    private void processResult(String json) {

        HistoryResult result = new Gson().fromJson(json, HistoryResult.class);
        HistoryPayload[] array = result.getPayload();
        Log.d("Test", array[0].getDestination());

        this.ui.callback(array);
    }

    interface HistoryTaskInterface {
        public void callback(HistoryPayload[] result);

        public void routeError();
    }
}
