package com.pppb.tubes_2;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;


import androidx.fragment.app.Fragment;

import com.pppb.tubes_2.databinding.FragmentLoginBinding;
import com.pppb.tubes_2.databinding.LoadingPopupBinding;

public class FragmentLogin extends Fragment implements View.OnClickListener, LoginTaskPresenter.PostCalculateInterface {
    private FragmentLoginBinding binding;
    private LoginTaskPresenter login;
    static String token;

    private LoadingPopupBinding binding_popup;
    private PopupWindow popupWindow;

    public static FragmentLogin newInstance(String title) {
        FragmentLogin fragment = new FragmentLogin();
        Bundle args = new Bundle();
        args.putString("title_edit", title);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.login = new LoginTaskPresenter(getActivity(), this);
        this.binding = FragmentLoginBinding.inflate(inflater, container, false);
        View view = this.binding.getRoot();
        this.binding.btnLogin.setOnClickListener(this);
        token = null;
        binding_popup = LoadingPopupBinding.inflate(inflater, container, false);
        return view;
    }


    @Override
    public void onClick(View view) {
        onButtonShowPopupWindowClick(view);
        String username = this.binding.etUsername.getText().toString();
        String password = this.binding.etPassword.getText().toString();
        this.login.execute(username, password);
    }

    @Override
    public void callback(String token, String message) {
        this.token = token;
        this.binding.etPassword.setText("");
        this.binding.etUsername.setText("");
        Log.d("token test 1", token);
        Bundle result = new Bundle();
        result.putInt("page", 2);
        getParentFragmentManager().setFragmentResult("changePage", result);
        this.closeSoftKeyBoard();
        popupWindow.dismiss();
    }

    @Override
    public void loginError() {
        Toast.makeText(getActivity(), "Password atau Username salah.",
                Toast.LENGTH_LONG).show();
        this.binding.etPassword.setText("");
        this.closeSoftKeyBoard();
        popupWindow.dismiss();
    }

    public void closeSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    public void onButtonShowPopupWindowClick(View view) {
        View popupView = this.binding_popup.getRoot();
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true;
        popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

    }
}
