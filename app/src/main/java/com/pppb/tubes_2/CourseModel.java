package com.pppb.tubes_2;

public class CourseModel {
    private String source;
    private String destination;
    private String vehicle;
    private String date;
    private String hour;

    public CourseModel(String source, String destination, String vehicle, String date, String hour){
        this.source=source;
        this.destination=destination;
        this.vehicle=vehicle;
        this.date=date;
        this.hour=hour;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public String getDate() {
        return date;
    }

    public String getHour() {
        return hour;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }
}
