package com.pppb.tubes_2;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;
@Parcel
public class RouteModel {
    public String source;
    public String destination;
    public String fee;
    @ParcelConstructor
    public RouteModel(String source , String destination, String fee){
        this.source=source;
        this.destination=destination;
        this.fee=fee;
    }

    public String getFee() {
        return fee;
    }

    public String getDestination() {
        return destination;
    }

    public String getSource() {
        return source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }
}