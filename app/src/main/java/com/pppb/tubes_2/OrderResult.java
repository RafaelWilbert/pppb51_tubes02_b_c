package com.pppb.tubes_2;

public class OrderResult {
    String message ;
    OrderPayload[] payload;
    public OrderResult (String message, OrderPayload[] payload){
        this.message =message;
        this.payload=payload;
    }

    public void setPayload(OrderPayload[] payload) {
        this.payload = payload;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public OrderPayload[] getPayload() {
        return payload;
    }
}
