package com.pppb.tubes_2;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class FragmentRoutesPresenter {
    private FragmentRoutesInterface ui;
    private String[] arr;
    public FragmentRoutesPresenter(FragmentRoutesInterface ui){
            this.ui=ui;
    }

    public void arrayNoDuplicate(String[] array, int j){
        array = new HashSet<String>(Arrays.asList(array)).toArray(new String[0]);
        Arrays.sort(array);

        this.ui.callback(array,  j);
    }


    interface FragmentRoutesInterface {
        public void callback(String[] array, int i);

    }
}
