package com.pppb.tubes_2;

public class OrderModel {
    String course_id;
    String seats;
    public OrderModel(String course_id,String seats){
        this.course_id = course_id;
        this.seats=seats;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getSeats() {
        return seats;
    }
}
