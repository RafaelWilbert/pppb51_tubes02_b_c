package com.pppb.tubes_2;

public class DetailPesananModel {
    String source ;
    String destination;
    String date;
    int[] seats;
    int fee;
    public DetailPesananModel(String source, String destination, String date, int[] seats, int fee){
        this.source=source;
        this.destination=destination;
        this.date= date;
        this.seats = seats;
        this.fee=fee;
    }

    public void setSeats(int[] seats) {
        this.seats = seats;
    }

    public int[] getSeats() {
        return seats;
    }

    public int getFee() {
        return fee;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

}
