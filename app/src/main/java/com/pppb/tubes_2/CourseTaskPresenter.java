package com.pppb.tubes_2;



import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class CourseTaskPresenter {

    private Context context;
    private Gson gson;
    private CourseTaskPresenter.GetCourseInterface ui;
    private JSONObject objJSON;
    public static String token;
    public CourseTaskPresenter(Context context, CourseTaskPresenter.GetCourseInterface ui) {
        this.context = context;
        this.ui = ui;
    }

    public void execute(CourseModel course) {
        Log.d("TAG", FragmentLogin.token);
        this.callVolley(objJSON, course);
    }

    private void callVolley(JSONObject toJson, CourseModel course) {
        String source= course.getSource();
        String destination = course.getDestination();;
        String vehicle=course.getVehicle();
        String date = course.getDate();
        String hour=course.getHour();
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        String BASE_URL = "https://devel.loconode.com/pppb/v1/courses"+"?source="+source+"&destination="+destination+"&vehicle="+vehicle+"&date="+date+"&hour="+hour;
//        BASE_URL="https://devel.loconode.com/pppb/v1/courses?source=Jakarta&destination=Bekasi&vehicle=Small&date=21-11-2022&hour=12";
        Log.d("url", BASE_URL);
        JsonObjectRequest myReq = new JsonObjectRequest(Request.Method.GET, BASE_URL, toJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        processResult(response.toString());
                    }
                },  new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Respon Error", error.toString());

                processError();

            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + FragmentLogin.token);
                return headers;
            }
        };
        requestQueue.add(myReq);
    }

    private void processError() {
        this.ui.routeError();
    }

    private void processResult(String json) {

        CourseResult result = new Gson().fromJson(json, CourseResult.class);
        getSeats[] array = result.getPayload();
        Log.d("Test",array[0].getDestination());

        this.ui.callback(array);
    }

    interface GetCourseInterface {
        public void callback(getSeats[] result);

        public void routeError();
    }
}
