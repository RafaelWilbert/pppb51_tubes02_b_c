package com.pppb.tubes_2;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OrderTaskPresenter {
    private final String BASE_URL = "https://devel.loconode.com/pppb/v1/orders";
    private Context context;
    private Gson gson;
    private OrderTaskPresenter.OrderTaskInterface ui;
    private JSONObject objJSON;
    public static String token;

    public OrderTaskPresenter(Context context, OrderTaskPresenter.OrderTaskInterface ui) {
        this.context = context;
        this.ui = ui;
        this.gson = new Gson();
    }

    public void execute(OrderModel order) {
        Log.d("TAG", FragmentLogin.token);
        try {
            objJSON = new JSONObject(gson.toJson(order));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.callVolley(objJSON);
    }

    private void callVolley(JSONObject toJson) {
        Log.d("url", BASE_URL);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL, toJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Respon", response.toString());
                        processResult(response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                processError();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + FragmentLogin.token);
                return headers;
            }
        };

        requestQueue.add(jsonRequest);
    }

    private void processError() {
        this.ui.routeError();
    }

    private void processResult(String json) {

        OrderResult result = new Gson().fromJson(json, OrderResult.class);
        OrderPayload[] array = result.getPayload();
        Log.d("Test", array[0].getOrder_id());

        this.ui.callback(array);
    }

    interface OrderTaskInterface {
        public void callback(OrderPayload[] payload);

        public void routeError();
    }
}

