package com.pppb.tubes_2;

public class HistoryResult {
    HistoryPayload[] payload;
    public HistoryResult(HistoryPayload[] payload){
        this.payload=payload;
    }

    public HistoryPayload[] getPayload() {
        return payload;
    }

    public void setPayload(HistoryPayload[] payload) {
        this.payload = payload;
    }
}
